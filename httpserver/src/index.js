const { Service } = require("./pico");

const prometheus = require('prom-client');

class Component {
  constructor({minValue, maxValue}) {
    let getRandomInt = (min, max) => Math.floor(Math.random() * (max - min + 1)) + min;

    this.min = minValue;
    this.max = maxValue;
    this.unit = "Something";
    this.value = null;
    this.B = Math.PI / 2;
    this.unitsTranslatedToTheRight = getRandomInt(0, 5);

  }
  amplitude() {
    return (this.max-this.min)/2;
  }
  unitsTranslatedUp() {
    return this.min + this.amplitude();
  }
  getLevel(t) {
    return this.amplitude() * Math.cos(this.B *(t-this.unitsTranslatedToTheRight)) + this.unitsTranslatedUp();
  }
}


let port = process.env.PORT || 9090;
let externalPort = process.env.EXTERNAL_PORT || port;

let service = new Service({});


let gauge01 = new prometheus.Gauge({ name: 'hop_la', help: 'my amazing hop_la metric' });
gauge01.set(0)

let gauge02 = new prometheus.Gauge({ name: 'bim_bam_boom', help: 'my amazing bim_bam_boom metric' });
gauge02.set(0)

let component01 = new Component({minValue:1.0, maxValue:10.0})
let component02 = new Component({minValue:-3.0, maxValue:4.0})

let gaugeTimer01 = setInterval(function() {
    let now = new Date();
    let t = now.getMinutes() + now.getSeconds() / 100;
    let componentValue = component01.getLevel(t);

    gauge01.inc(componentValue)

}, 1000)  

let gaugeTimer02 = setInterval(function() {
    let now = new Date();
    let t = now.getMinutes() + now.getSeconds() / 100;
    let componentValue = component02.getLevel(t);
    
    gauge02.inc(componentValue)

}, 2000)  


function formatText(text) { return text.split("\n").map(item => item.trim()).join("\n") }

service.get({uri: `/metrics`,f: (request, response) => {
  response.send(
    contentType=prometheus.register.contentType, 
    content=prometheus.register.metrics()
  )
}})

service.get({
  uri: `/`,
  f: (request, response) => {
    response.sendHtml(`
    <!doctype html>
    <html>
      <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title>Hello World!</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            .container { min-height: 100vh; display: flex; justify-content: center; align-items: center; text-align: center; }
            .title { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; display: block; font-weight: 300; font-size: 100px; color: #35495e; letter-spacing: 1px; }
            .subtitle { font-family: "Source Sans Pro", "Helvetica Neue", Arial, sans-serif; font-weight: 300; font-size: 42px; color: #526488; word-spacing: 5px; padding-bottom: 15px; }
            .links { padding-top: 15px; }
        </style>
      </head>
      <body>
        <section class="container">
          <div>
            <h1 class="title">
              👋 Hello 🌍 World
            </h1>
            <h2 class="subtitle">
              made with ❤️
            </h2>          
          </div>
        </section>
      </body>
    </html>  
  `);
  }
});

service.start({
  port: port
}, res => {
  res.when({
    Failure: error => console.log("😡 Houston? We have a problem!", error),
    Success: port => console.log(`🌍 pico service is listening on ${port}`)
  });
});